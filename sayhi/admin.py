from django.contrib import admin
from .models import Letter, Profile

# Register your models here.
admin.site.register(Letter)
admin.site.register(Profile)
