from tkinter import CASCADE

from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class Letter(models.Model):
    user = models.ForeignKey(User, on_delete=CASCADE)
    text = models.TextField()
    deliver = models.DateField()
    from_mail = models.EmailField()
    is_public = models.BooleanField(default=False)
    is_sent = models.BooleanField(default=False)

    def __str__(self):
        return self.from_mail


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=CASCADE)
    is_vip = models.BooleanField(default=False)
    picture = models.ImageField(upload_to='profile_images', blank=True)

