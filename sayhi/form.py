from django import forms
from .models import Letter, Profile, User
from jalali_date.fields import JalaliDateField, SplitJalaliDateTimeField
from jalali_date.widgets import AdminJalaliDateWidget, AdminSplitJalaliDateTime


class SendLetter(forms.ModelForm):
    text = forms.CharField(widget=forms.Textarea, label='نامه شما برای آینده')
    deliver = forms.DateField(widget=forms.SelectDateWidget, label='تحویل نامه در')
    from_mail = forms.EmailField(label='پست‌الکترونیکی')
    is_public = forms.BooleanField(widget=forms.CheckboxInput,
                                   label="این نامه بدون نام من و به صورت عمومی هم منتشر بشه!", required=False)

    class Meta:
        model = Letter
        fields = ('text', 'deliver', 'from_mail', 'is_public')
        exclude = ('User',)

    def __init__(self, *args, **kwargs):
        super(SendLetter, self).__init__(*args, **kwargs)
        self.fields['deliver'] = JalaliDateField(label='تحویل نامه در',  # date format is  "yyyy-mm-dd"
                                                 widget=AdminJalaliDateWidget  # optional, to use default datepicker
                                                 )