from django.shortcuts import render, reverse
from django.views.generic import ListView, View, DetailView
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from .form import SendLetter
from django.contrib.auth.decorators import login_required
from .models import Letter
from django.core.mail import EmailMessage
from django.template.loader import get_template
from django.utils import timezone
from datetime import date, datetime
import random


# Create your views here.

def index(request):
    send_emails()
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('new_letter'))
    return render(request, 'sayhi/index.html')


@login_required
def new_letter(request):
    form = SendLetter()
    if request.method == 'POST':
        form = SendLetter(request.POST)
        if form.is_valid():
            letter = form.save(commit=False)
            letter.user = request.user
            letter.save()
            return HttpResponseRedirect(reverse('index'))
    return render(request, 'sayhi/new_letter.html', {'form': form})


class PublicLetters(ListView):
    template_name = 'sayhi/public_letters.html'
    context_object_name = 'letters'

    def get_queryset(self):
        return Letter.objects.filter(is_public=True)


def donate(request):
    return render(request, 'sayhi/donate.html')


class LettersView(DetailView):
    model = User
    template_name = 'sayhi/show_letters.html'


def send_emails():
    all_letters = Letter.objects.filter(is_sent=False)
    # Email the profile with the
    # contact information
    template = get_template('sayhi/send_letter.txt')
    for letter in all_letters:
        deliver = datetime.strptime(str(letter.deliver), '%Y-%m-%d')
        if (datetime.today() - deliver).days > 0:
            random_number = random.randint(1, User.objects.count())
            random_user = User.objects.get(pk=random_number)
            context = {
                'from_mail': letter.from_mail,
                'text': letter.text,
            }
            letter_mail = template.render(context)

            email = EmailMessage(
                "یک نامه از گذشته",
                letter_mail,
                "hi@futurehi.ir" + '',
                [random_user.email],
                headers={'Reply-To': letter.from_mail}
            )
            email.send()
            letter.is_sent = True
